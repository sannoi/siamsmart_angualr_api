﻿using siamsmartcollection_tenant.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace siamsmartcollection_tenant.App_Code
{
    public class sentmail
    {
        EmailTemplateDLL Serv_2 = new EmailTemplateDLL();
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");

        string E_Link = "";
        string E_Sendfromteam = "";
        string E_Sendfromcompany = "";
        string E_Tel = "";
        string E_F_Email = "";
        string E_F_Callcenter = "";
        string codetheme = "";

        protected string MyTheme { get; set; } //btn
        protected string NavbarColor { get; set; } //txtbtn



        public void CallMail_forget_password(string Dear, string CompanyName, string ShopName, string BusinessPartner, string floor, string room, string DateNow,
            string email, string companycode)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(companycode);
            {
                if (Email_template_by_CompanyCode.Rows.Count != 0)
                {
                    E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                    E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                    E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                    E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                    E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                    E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                    codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                }
            }

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear " + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>เนื่องจากร้านค้าผู้เช่าไม่สามารถจำ User หรือ Password ในการนำส่งยอดขายได้ " +
                    "ระบบจึงทำการแจ้งเตือนว่ายังคงอนุญาตให้ร้านค้าผู้เช่าทำการบักทึกยอดขายต่อไปได้</t>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>กรุณาทำการ Reset Password ร้านค้าผู้เช่าดังกล่าว</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append("<tr>");
                stringBuilder.Append("  <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + CompanyName + "</h4></td>");
                stringBuilder.Append("  <td></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชื่อร้าน</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + ShopName + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชื่อบริษัท</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + BusinessPartner + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ชั้น</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + floor + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>ห้อง</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + room + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>วันที่ Login โดยไม่มีการระบุ User and Password</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + DateNow + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Link</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + link_ + "</p></td>");
                stringBuilder.Append(" </tr>");

                stringBuilder.Append("    </table>");


                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + " " + E_Sendfromcompany + " โทร  " + E_Tel + " </p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "/t> | IT Call Center " + E_F_Callcenter + "</p></center>");
                stringBuilder.Append("</div>");



                stringBuilder.Append("</body>");

                var emailMessage = new MailMessage
                {
                    Subject = "ระบบ Tenant Sales Data Collection  : Forgot Password  ร้าน " + ShopName + " ห้อง " + room + "",
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string receiver = "";

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {
                        if (receiver.Contains(allmail[xxx].Trim()) == false)
                        {
                            receiver = receiver + allmail[xxx].Trim() + ";";
                            emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                        }
                    }
                }

                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);
            }
            catch
            {
                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }


        public void CallMail_adjust(string email, string subject, string Header, string Dear, string Title,
         string label1, string label2, string label3, string label4, string label5, string label6, string label7,
         string detail1, string detail2, string detail3, string detail4, string detail5, string detail6, string detail7, string link, string companycode)
        {

            string link_ = ConfigurationManager.AppSettings["link_path"];

            var Email_template_by_CompanyCode = Serv_2.getTemplate_by_CompanyCode(companycode);
            {
                if (Email_template_by_CompanyCode.Rows.Count != 0)
                {
                    E_Link = Email_template_by_CompanyCode.Rows[0]["Link"].ToString();
                    E_Sendfromteam = Email_template_by_CompanyCode.Rows[0]["Sendfromteam"].ToString();
                    E_Sendfromcompany = Email_template_by_CompanyCode.Rows[0]["Sendfromcompany"].ToString();
                    E_Tel = Email_template_by_CompanyCode.Rows[0]["Tel"].ToString();
                    E_F_Email = Email_template_by_CompanyCode.Rows[0]["F_Email"].ToString();
                    E_F_Callcenter = Email_template_by_CompanyCode.Rows[0]["F_Callcenter"].ToString();
                    codetheme = Email_template_by_CompanyCode.Rows[0]["ButtonColor"].ToString();
                }
            }

            //var exchangeService = new ExchangeService(ExchangeVersion.Exchange2010)
            //{
            //    Credentials = new WebCredentials("smartcollection", "Sm@rt2018", "spwg"),
            //    Url = new Uri("https://webmail.siampiwat.com/EWS/Exchange.asmx")
            //};

            try
            {
                var stringBuilder = new StringBuilder();

                stringBuilder.Append("<body>");
                stringBuilder.Append("<div style='width:100%; height: 70px;background-color:" + codetheme + "; margin:0px;'>");
                stringBuilder.Append("<center><h1 style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>" + Header + "</h1></center>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='width: 80 %; margin - top: 40px; margin - left: 10 %; '>");
                stringBuilder.Append("<h3>Dear " + Dear + " ,</h3>");
                stringBuilder.Append("<t style='margin - top:20px; font - size:14px; '>" + Title + "</t>");
                stringBuilder.Append("</div>");
                stringBuilder.Append("<div style='position: absolute; width: 30 %; height: 370px; margin - left:10 %; color:#cc5600;'>");

                stringBuilder.Append("<table style='width:55%; height: 370px; margin-left:25%; margin-top:15px;'>");
                stringBuilder.Append("<tr>");
                stringBuilder.Append("  <td style='width:40%; color: " + codetheme + " ;'><h4 style='margin:5px; margin-left:0px;'>" + label1 + "</h4></td>");

                if (detail1 == "")
                {
                    stringBuilder.Append("  <td></td>");

                }
                else
                {
                    stringBuilder.Append("  <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail1 + "</p></td>");

                }

                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("   <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label2 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail2 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label3 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail3 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("  <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label4 + "</h4></td>");
                stringBuilder.Append("    <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail4 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("     <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label5 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail5 + "</p></td>");
                stringBuilder.Append("  </tr>");
                stringBuilder.Append(" <tr>");
                stringBuilder.Append("    <td style='width:40%;'><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>" + label6 + "</h4></td>");
                stringBuilder.Append("     <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + detail6 + "</p></td>");
                stringBuilder.Append(" </tr>");
                stringBuilder.Append("  <tr>");
                stringBuilder.Append("     <td style='width:40%; '><h4 style='margin:5px; margin-left:0px;color:" + codetheme + ";'>Remark</h4></td>");
                stringBuilder.Append("   <td><p style='margin:5px; margin-left:0px; border-bottom: 1px dotted #717372; color:#717372;'>:  " + link_ + "</p></td>");
                stringBuilder.Append("  </tr>");


                stringBuilder.Append("    </table>");


                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>จึงเรียนมาเพื่อทราบ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>" + E_Sendfromteam + " " + E_Sendfromcompany + " โทร " + E_Tel + " </p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("   <div style='width:80%;  margin-left: 20%;'>");
                stringBuilder.Append("      <p style='font-size:17px;'>อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ</p>");
                stringBuilder.Append("   </div>");

                stringBuilder.Append("<div style='width:80%; margin-top: 10px; margin-left: 8%;'>");
                stringBuilder.Append("<p style='font-size:17px;'>Please click this <a href=" + E_Link + ">LINK</a> to View and complete your active task.</p>");
                stringBuilder.Append("</div>");

                stringBuilder.Append("<div style='width:100%; height: 40px;background:#4b4c44; margin:0px;'>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 10px;color:#cbcbcb;'>Tenant Sales Data Collection</p></center>");
                stringBuilder.Append("<center><p style='padding:0px; margin:0px; margin-top: 3px;color:#cbcbcb;'>Should you need any support, please contact <t style='color:#fff;'>" + E_F_Email + "</t> | IT Call Center " + E_F_Callcenter + "</p></center>");
                stringBuilder.Append("</div>");


                stringBuilder.Append("</body>");

                //ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                //{
                //    return true;
                //};


                var emailMessage = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress("noreply-SiamSmartCollection@siampiwat.com", "noreply")
                };

                string receiver = "";

                string[] allmail = email.Replace(" ", "").Split(',');
                if (allmail.Length != 0)
                {
                    for (int xxx = 0; xxx < allmail.Length; xxx++)
                    {

                        if (receiver.Contains(allmail[xxx].Trim()) == false)
                        {
                            receiver = receiver + allmail[xxx].Trim() + ";";
                            emailMessage.To.Add(new MailAddress(allmail[xxx].Trim()));
                        }
                    }
                }


                emailMessage.IsBodyHtml = true;
                emailMessage.Body = stringBuilder.ToString();

                var smtpClient = new SmtpClient("smtprelay.siampiwat.com");
                smtpClient.Send(emailMessage);

            }
            catch
            {
                throw;
            }
            finally
            {
                //exchangeService = null;
            }


        }

        public DataTable GetUserAR_bcc(string contractnumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select u.username ,u.email from tblsmart_Contract s inner " +
                        " join tbluser u on s.smart_icon_staff_id = u.userid " +
                        " where s.contractnumber = '" + contractnumber + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

    }

}