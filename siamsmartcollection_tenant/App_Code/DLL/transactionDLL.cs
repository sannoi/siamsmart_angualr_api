﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace siamsmartcollection_tenant.App_Code.DLL
{
    public class transactionDLL
    {
        public DataTable Get_Confirm_new(string CompanyCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tbljob where job_name = 'jobconfirm'  and CompanyCode = @CompanyCode ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@CompanyCode", CompanyCode);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Get_Confirm_new2(string CompanyCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from tbljob where job_name = 'jobconfirm2'   and CompanyCode = @CompanyCode ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@CompanyCode", CompanyCode);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getOldContract(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += " select * from tblsmart_Contract where ContractNumber = @ContractNumber and Contract_old_number != '' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetTransaction(string ContractNumber, string d1, string d2, string status)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            var _with1 = objCmd;

            d1 = d1 + " 00:00:00";
            d2 = d2 + " 23:59:59";
            string[] contractnumber_list = ContractNumber.ToString().Split(',');

            if (status == "")
            {
                strSQL += "select t.id, t.record_date ,t.ContractNumber,t.product_amount,t.service_serCharge_amount, case " +

                    " when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' " +
                    " when t.status = 'y' then 'ส่งยอดแล้ว' " +

                    " else 'ยังไม่ส่งยอด' end as status , " +
                    "  case when t.flag_confirm = 'y' then 'ยืนยันยอดแล้ว' else 'ยังไม่ยืนยันยอด' end as flag_confirm  " +
                    " from tbltransaction_record t where " +
                    " t.record_date between CONVERT(datetime, @d1)  and CONVERT(datetime, @d2 ) " +
                    " and ContractNUmber in  ( ";

                int i = 1;
                foreach (var contract in contractnumber_list)
                {
                    strSQL += "@ContractNumber" + i + ",";
                    _with1.Parameters.AddWithValue("@ContractNumber" + i, contract);
                    i++;
                }

                strSQL = strSQL.Substring(0, strSQL.Length - 1);


                strSQL += " ) order by  t.record_date ";
            }
            else
            {
                if (status == "c" || status == "nc")
                {
                    string s;
                    if (status == "c")
                    {
                        status = "y";
                    }
                    else
                    {
                        status  = "n";
                    }

                    strSQL += "select t.id, t.record_date ,t.ContractNumber,t.product_amount,t.service_serCharge_amount, case " +

                    " when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' " +
                    " when t.status = 'y' then 'ส่งยอดแล้ว' " +

                        " else 'ยังไม่ส่งยอด' end as status , " +
                  "  case when t.flag_confirm = 'y' then 'ยืนยันยอดแล้ว' else 'ยังไม่ยืนยันยอด' end as flag_confirm  " +
                  " from tbltransaction_record t where " +
                  " t.record_date between CONVERT(datetime, @d1)  and CONVERT(datetime,@d2 ) and t.flag_confirm = @status " +
                  " and ContractNUmber in  ( ";

                    int i = 1;
                    foreach (var contract2 in contractnumber_list)
                    {
                        strSQL += "@ContractNumber" + i + ",";
                        _with1.Parameters.AddWithValue("@ContractNumber" + i, contract2);
                        i++;
                    }

                    strSQL = strSQL.Substring(0, strSQL.Length - 1);

                    strSQL += " ) order by  t.record_date ";
                }
                else if (status == "a")
                {
                    strSQL += "select t.id, t.record_date ,t.ContractNumber,t.product_amount,t.service_serCharge_amount, case " +

                    " when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' " +
                    " when t.status = 'y' then 'ส่งยอดแล้ว' " +

                        " else 'ยังไม่ส่งยอด' end as status , " +
                  "  case when t.flag_confirm = 'y' then 'ยืนยันยอดแล้ว' else 'ยังไม่ยืนยันยอด' end as flag_confirm  " +
                  " from tbltransaction_record t where " +
                  " t.record_date between CONVERT(datetime, @d1)  and CONVERT(datetime,@d2 ) and t.flag_adjust = 'y' " +
                  " and ContractNUmber in  ( ";

                    int i = 1;
                    foreach (var contract3 in contractnumber_list)
                    {
                        strSQL += "@ContractNumber" + i + ",";
                        _with1.Parameters.AddWithValue("@ContractNumber" + i, contract3);
                        i++;
                    }

                    strSQL = strSQL.Substring(0, strSQL.Length - 1);

                    strSQL += " ) order by  t.record_date ";
                }
                else
                {
                    strSQL += "select t.id, t.record_date ,t.ContractNumber,t.product_amount,t.service_serCharge_amount, case " +

                    " when t.flag_adjust = 'y' then 'ขอแก้ไขยอด' " +
                    " when t.status = 'y' then 'ส่งยอดแล้ว' " +

                        " else 'ยังไม่ส่งยอด' end as status , " +
                  "  case when t.flag_confirm = 'y' then 'ยืนยันยอดแล้ว' else 'ยังไม่ยืนยันยอด' end as flag_confirm  " +
                  " from tbltransaction_record t where " +
                  " t.record_date between CONVERT(datetime, @d1)  and CONVERT(datetime,@d2 ) and t.status = @status " +
                  " and ContractNUmber in  ( ";

                    int i = 1;
                    foreach (var contract4 in contractnumber_list)
                    {
                        strSQL += "@ContractNumber" + i + ",";
                        _with1.Parameters.AddWithValue("@ContractNumber" + i, contract4);
                        i++;
                    }

                    strSQL = strSQL.Substring(0, strSQL.Length - 1);

                    strSQL += " ) order by  t.record_date ";

                }
            }


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@d1", d1);
            _with1.Parameters.AddWithValue("@d2", d2);
            _with1.Parameters.AddWithValue("@status", status);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void Update_flagConfirm(string ContractNumber, string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " update tbltransaction_record set  flag_confirm = 'y'  where id = @id  and contractnumber = @ContractNumber ; ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
            _with1.Parameters.AddWithValue("@id", id);
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetContract(string ContractNumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += " select s.id, c.CompanyNameTH, s.BusinessPartnerName, s.ShopName, s.smart_room_no ,  s.CompanyCode, s.ContractNumber,s.smart_record_keyin_type, " +
                        " case when s.smart_record_person_type = 'te' then 'Tenant' else 'AR' end as record_person , s.smart_record_person_type, " +
                        " s.smart_icon_staff_id,s.* " +
                        " from tblsmart_Contract s inner " +
                        " join SAPCompany c on s.CompanyCode = c.CompanyCode " +
                        " where s.ContractNumber = @ContractNumber ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetTransaction_detail(string id, string contractnumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += " select * from tbltransaction_record where id = @id  and contractnumber = @contractnumber ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@id", id);
            _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetTransactionByDate(string record_date, string contractnumber)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            string record_date_1 = record_date + " 00:00:00";
            string record_date_2 = record_date + " 23:59:59";

            strSQL += " select * from tbltransaction_record where record_date between CONVERT(datetime, @record_date_1)  and CONVERT(datetime, @record_date_2 ) " +
                " and ContractNumber = @contractnumber ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@record_date_1", record_date_1);
            _with1.Parameters.AddWithValue("@record_date_2", record_date_2);
            _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void Update_transaction_recor_1(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
  string record_date, string no_type, string remark, string id, string update_date, string update_id, int vat, string update_role, string key_in_channel)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;



            decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

            decimal v2 = 0;

            if (service_serCharge_amount != "0")
            {
                if (service_serCharge_amount != "")
                {
                    v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
                }
            }




            strSQL = " Update tbltransaction_record set " +
                " product_amount = @product_amount , " +
                " service_serCharge_amount = @service_serCharge_amount , " +
                " status = 'y' , record_date = @record_date , " +
                " no_type = @no_type ,remark = @remark " +

                " , update_date = @update_date ,update_id = @update_id  , update_role = @update_role " +

                " , product_amount_vat = @v1 , service_serCharge_amount_vat = @v2 " +

                " , key_in_channel = @key_in_channel " +

                " where ContractNumber  = @ContractNumber and id = @id ";



            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@product_amount", product_amount);
            _with1.Parameters.AddWithValue("@service_serCharge_amount", service_serCharge_amount);
            _with1.Parameters.AddWithValue("@record_date", record_date);
            _with1.Parameters.AddWithValue("@no_type", no_type);
            _with1.Parameters.AddWithValue("@remark", remark);
            _with1.Parameters.AddWithValue("@update_date", update_date);
            _with1.Parameters.AddWithValue("@update_id", update_id);
            _with1.Parameters.AddWithValue("@update_role", update_role);
            _with1.Parameters.AddWithValue("@v1", v1);
            _with1.Parameters.AddWithValue("@v2", v2);
            _with1.Parameters.AddWithValue("@key_in_channel", key_in_channel);
            _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
            _with1.Parameters.AddWithValue("@id", id);
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


        public void Update_transaction_recor_createDate_Stamp(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
              string record_date, string no_type, string remark, string id, string update_date, string update_id, int vat, string create_role, string key_in_channel)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;



            decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

            decimal v2 = 0;

            if (service_serCharge_amount != "0")
            {
                if (service_serCharge_amount != "")
                {
                    v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
                }
            }




            strSQL = " Update tbltransaction_record set " +
                " product_amount = @product_amount , " +
                " service_serCharge_amount = @service_serCharge_amount , " +
                " status = 'y' , record_date = @record_date , " +
                " no_type = @no_type ,remark = @remark  " +

                " , create_date = @update_date ,create_id = @update_id  , create_role  = @create_role " +

                " , product_amount_vat = @v1 , service_serCharge_amount_vat = @v2 " +
                " , key_in_channel = @key_in_channel " +

                " where ContractNumber  = @ContractNumber and id = @id ";



            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@product_amount", product_amount);
            _with1.Parameters.AddWithValue("@service_serCharge_amount", service_serCharge_amount);
            _with1.Parameters.AddWithValue("@record_date", record_date);
            _with1.Parameters.AddWithValue("@no_type", no_type);
            _with1.Parameters.AddWithValue("@remark", remark);
            _with1.Parameters.AddWithValue("@update_date", update_date);
            _with1.Parameters.AddWithValue("@update_id", update_id);
            _with1.Parameters.AddWithValue("@create_role", create_role);
            _with1.Parameters.AddWithValue("@v1", v1);
            _with1.Parameters.AddWithValue("@v2", v2);
            _with1.Parameters.AddWithValue("@key_in_channel", key_in_channel);
            _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
            _with1.Parameters.AddWithValue("@id", id);
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable Insert_transaction_record(string ContractNumber, string product_amount, string service_serCharge_amount, string status,
       string record_date, string no_type, string remark, int vat, string type, string create_id, string create_role, string key_in_channel)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            decimal service_serCharge_amount_ = 0;

            if (service_serCharge_amount != "")
            {
                service_serCharge_amount_ = Convert.ToDecimal(service_serCharge_amount);
            }
            else
            {
                service_serCharge_amount_ = 0;
            }

            decimal v1 = (Convert.ToDecimal(product_amount) * vat) / (100 + vat);

            decimal v2 = 0;

            if (type == "prod_service")
            {
                if (service_serCharge_amount != "")
                {
                    v2 = (Convert.ToDecimal(service_serCharge_amount) * vat) / (100 + vat);
                }
            }




            strSQL = " insert into tbltransaction_record(ContractNumber,product_amount,service_serCharge_amount,status,record_date,no_type,remark," +
                " product_amount_vat,service_serCharge_amount_vat,create_date,create_id,create_role, key_in_channel ) " +
                        " values(@ContractNumber , @product_amount, @service_serCharge_amount_, @status , @record_date , " +
                        " @no_type , @remark , @v1 , @v2 , getdate() , @create_id , @create_role ,  @key_in_channel ); " +
                        " SELECT @@identity as last_id ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@ContractNumber", ContractNumber);
            _with1.Parameters.AddWithValue("@product_amount", product_amount);
            _with1.Parameters.AddWithValue("@service_serCharge_amount_", service_serCharge_amount_);
            _with1.Parameters.AddWithValue("@status", status);
            _with1.Parameters.AddWithValue("@record_date", record_date);
            _with1.Parameters.AddWithValue("@no_type", no_type);
            _with1.Parameters.AddWithValue("@remark", remark);
            _with1.Parameters.AddWithValue("@v1", v1);
            _with1.Parameters.AddWithValue("@v2", v2);
            _with1.Parameters.AddWithValue("@create_id", create_id);
            _with1.Parameters.AddWithValue("@create_role", create_role);
            _with1.Parameters.AddWithValue("@key_in_channel", key_in_channel);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;

        }

        public DataTable GetVAT(string CompanyCode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += " select * from tblsetting_vat where CompanyCode = @CompanyCode ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@CompanyCode", CompanyCode);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void tbltransaction_record_img(string transaction_id, string img, string filename, string filename_display)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " insert into tbltransaction_record_img(transaction_id,img,filename,filename_display) " +
                        " values( @transaction_id , @img , @filename , @filename_display ) ; ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@transaction_id", transaction_id);
            _with1.Parameters.AddWithValue("@img", img);
            _with1.Parameters.AddWithValue("@filename", filename);
            _with1.Parameters.AddWithValue("@filename_display", filename_display);
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetTransaction_detail_img(string trans_id)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += " select * from tbltransaction_record_img where transaction_id = @trans_id  ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@trans_id", trans_id);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable GetTransaction_detail_imgByID(string id)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += " select * from tbltransaction_record_img where id = @id  ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@id", id);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetTransaction_detail_edit_img(string trans_id)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += " select TOP 2* from tbledit_transaction_record_img where transaction_id = @trans_id  ORDER BY id DESC ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@trans_id", trans_id);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetTransaction_detail_edit_imgByID(string id)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += " select TOP 1 * from tbledit_transaction_record_img where id = @id  ORDER BY id DESC ; ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@id", id);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public void Update_Transaction(string adjust_remark, string id, string contractnumber, string request_adj_id, string request_adj_role)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;





            strSQL = " update tbltransaction_record set adjust_remark = @adjust_remark , flag_adjust = 'y', request_adj_id = @request_adj_id , request_adj_date = getdate(), " +
                " request_adj_role = @request_adj_role ,flag_confirm = 'n' where id = @id  and contractnumber = @contractnumber  ; ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@adjust_remark", adjust_remark);
            _with1.Parameters.AddWithValue("@request_adj_id", request_adj_id);
            _with1.Parameters.AddWithValue("@request_adj_role", request_adj_role);
            _with1.Parameters.AddWithValue("@id", id);
            _with1.Parameters.AddWithValue("@contractnumber", contractnumber);
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public DataTable GetUser(string userid)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";


            strSQL += "  select * from tbluser where userid = @userid ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@userid", userid);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable GetCompanyCodeByCompanyCode(string companycode)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "select * from SAPCompany where CompanyCode = @companycode  and IsActive = '1' ";


            objConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ConnectionString;
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@companycode", companycode);
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void tbl_edit_transaction_record_img(string transaction_id, string img, string filename, string filename_display, string userid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;


            strSQL = " insert into tbledit_transaction_record_img(transaction_id,img,filename,filename_display,create_id,create_date,update_id,update_date) " +
                        " values(@transaction_id , @img , @filename , @filename_display , @userid ,getdate(),'',getdate()) ; ";

            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Smart_CollectionConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.Parameters.AddWithValue("@transaction_id", transaction_id);
            _with1.Parameters.AddWithValue("@img", img);
            _with1.Parameters.AddWithValue("@filename", filename);
            _with1.Parameters.AddWithValue("@filename_display", filename_display);
            _with1.Parameters.AddWithValue("@userid", userid);
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

    }
}