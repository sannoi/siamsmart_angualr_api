﻿using Newtonsoft.Json;
using siamsmartcollection_tenant.App_Code;
using siamsmartcollection_tenant.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace siamsmartcollection_tenant.Controllers
{
    public class loginController : ApiController
    {
        public class TokenRequest
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        public class TokenRequest_email
        {
            public string email { get; set; }
        }


        public class TokenResponse
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public int expires_in { get; set; }
            public string userName { get; set; }
            public DateTime issued { get; set; }
            public DateTime expires { get; set; }
            public string error { get; set; }
            public string error_description { get; set; }
            public string contractnumber { get; set; }
            public string Company { get; set; }
            public string Customername { get; set; }
            public string Shopname { get; set; }
            public string Roomnumber { get; set; }
            public string User_id { get; set; }
            public string code_theme { get; set; }
            public string code_Navbar { get; set; }
            public string code_Bg { get; set; }
            public string code_Bg2 { get; set; }
            public string code_Bg3 { get; set; }
            public string code_Bg4 { get; set; }
            public string code_Bg5 { get; set; }
            public string code_Navbar2 { get; set; }
            public string PicGroupCompany { get; set; }
            public string returncode { get; set; }
            public string desc { get; set; }

        }

        public class ret_logo
        {
            public string image1 { get; set; }
            public string image2 { get; set; }
            public string image3 { get; set; }
            public string returncode { get; set; }
            public string desc { get; set; }
        }

        [HttpPost]
        [Route("api/GetToken")]
        public async Task<IHttpActionResult> GetToken(TokenRequest request)
        {
            var client = new HttpClient()
            {
                BaseAddress = new Uri(Request.RequestUri.GetLeftPart(UriPartial.Authority))
            };

            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", request.username),
                new KeyValuePair<string, string>("password", request.password)
            });

            var result = await client.PostAsync("/token", content);
            //var result = await client.PostAsync("http://18.141.84.70/168x/token", content);


            string resultContent = await result.Content.ReadAsStringAsync();
            resultContent = resultContent.Replace(".issued", "issued").Replace(".expires", "expires");
            TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(resultContent);

            try
            {
                loginDLL dll = new loginDLL();
                DataTable dt = new DataTable();
                dt = dll.getUser_tenant(request.username, request.password);

                if (dt.Rows.Count != 0)
                {
                    tokenResponse.User_id = dt.Rows[0]["id"].ToString();
                    dll.Insert_log_login(dt.Rows[0]["id"].ToString(), "tenant", "web");

                    var comcode = dll.getComcode_by_ContractNumber(dt.Rows[0]["contractnumber"].ToString());
                    //string comcode = u.Rows[0]["contractnumber"].ToString().Substring(0, 3);
                    if (comcode.Rows.Count != 0)
                    {
                        tokenResponse.userName = request.username;

                        var theme = dll.getThemeUser_tenant(comcode.Rows[0]["CompanyCode"].ToString());
                        if (theme.Rows.Count != 0)
                        {
                            tokenResponse.code_theme = theme.Rows[0]["ButtonColor"].ToString();
                            tokenResponse.code_Navbar = theme.Rows[0]["NavbarColor"].ToString();
                            tokenResponse.code_Bg = theme.Rows[0]["BgUri"].ToString();
                            tokenResponse.code_Bg2 = theme.Rows[0]["BgUri2"].ToString();
                            tokenResponse.code_Bg3 = theme.Rows[0]["BgUri3"].ToString();
                            tokenResponse.code_Bg4 = theme.Rows[0]["BgUri4"].ToString();
                            tokenResponse.code_Bg5 = theme.Rows[0]["BgUri5"].ToString();
                            tokenResponse.code_Navbar2 = theme.Rows[0]["NavbarColor2"].ToString();
                            tokenResponse.PicGroupCompany = theme.Rows[0]["CompanyGroupLogo"].ToString();
                            tokenResponse.contractnumber = dt.Rows[0]["contractnumber"].ToString();
                            tokenResponse.returncode = "1000";

                            var contract = dll.GetContract(dt.Rows[0]["contractnumber"].ToString());
                            if (contract.Rows.Count != 0)
                            {
                                tokenResponse.Company = contract.Rows[0]["CompanyNameTH"].ToString();
                                tokenResponse.Customername = contract.Rows[0]["BusinessPartnerName"].ToString();
                                tokenResponse.Shopname = contract.Rows[0]["ShopName"].ToString();
                                tokenResponse.Roomnumber = contract.Rows[0]["smart_room_no"].ToString();
                            }

                        }
                        else
                        {
                            tokenResponse.code_theme = "#CCA9DA";
                            tokenResponse.code_Navbar = "#fff";
                            tokenResponse.code_Bg = "#2d2339";
                            tokenResponse.code_Bg2 = "#453658";
                            tokenResponse.code_Bg3 = "#5f4a79";
                            tokenResponse.code_Bg4 = "#7b5f9c";
                            tokenResponse.code_Bg5 = "#9775c0";
                            tokenResponse.code_Navbar2 = "#fff";
                            tokenResponse.PicGroupCompany = "img/siampi.png";
                        }
                    }

                }
                else
                {
                    tokenResponse.returncode = "3800";

                }
            }
            catch (Exception ex)
            {
                tokenResponse.Company = ex.ToString();
            }

            return Ok(tokenResponse);
        }


        [HttpPost]
        [Route("api/GetTokenByEmail")]
        public async Task<IHttpActionResult> GetTokenByEmail(TokenRequest_email request)
        {
            string username = "";
            string password = "";
            CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
            sentmail mail = new sentmail();

            loginDLL dll = new loginDLL();
            TokenResponse tokenResponse = new TokenResponse();

            var te = dll.GetUserByEmail(request.email);
            if (te.Rows.Count > 1)
            {
                tokenResponse.returncode = "3800";
                tokenResponse.desc = "ไม่สามารถใช้งานฟังก์ชั่นนี้ได้ เนื่องจากมีการผูกอีเมล์หลายร้านค้า กรุณาติดต่อ AR team";
            }
            else
            {
                var u = dll.GetUserByContractnumber(te.Rows[0]["contractnumber"].ToString());
                if (u.Rows.Count != 0)
                {
                    username = u.Rows[0]["username"].ToString();
                    password = u.Rows[0]["d_password"].ToString();

                    var client = new HttpClient()
                    {
                        BaseAddress = new Uri(Request.RequestUri.GetLeftPart(UriPartial.Authority))
                    };

                    var content = new FormUrlEncodedContent(new[]
                    {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", username),
                    new KeyValuePair<string, string>("password", password)
                });

                    var result = await client.PostAsync("/token", content);


                    string resultContent = await result.Content.ReadAsStringAsync();
                    resultContent = resultContent.Replace(".issued", "issued").Replace(".expires", "expires");
                    tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(resultContent);

                    try
                    {
                        DataTable dt = new DataTable();
                        dt = dll.getUser_tenant(username, password);

                        if (dt.Rows.Count != 0)
                        {

                            tokenResponse.userName = username;

                            string email_ar = "";
                            string ar_name = "";
                            string shop_name = "";
                            string room_number = "";
                            string business_name = "";
                            string co_name = "";
                            string floor = "";

                            tokenResponse.User_id = dt.Rows[0]["id"].ToString();
                            dll.Insert_log_login(dt.Rows[0]["id"].ToString(), "tenant", "web");

                            var comcode = dll.getComcode_by_ContractNumber(dt.Rows[0]["contractnumber"].ToString());
                            //string comcode = u.Rows[0]["contractnumber"].ToString().Substring(0, 3);
                            if (comcode.Rows.Count != 0)
                            {
                                var theme = dll.getThemeUser_tenant(comcode.Rows[0]["CompanyCode"].ToString());
                                if (theme.Rows.Count != 0)
                                {
                                    tokenResponse.code_theme = theme.Rows[0]["ButtonColor"].ToString();
                                    tokenResponse.code_Navbar = theme.Rows[0]["NavbarColor"].ToString();
                                    tokenResponse.code_Bg = theme.Rows[0]["BgUri"].ToString();
                                    tokenResponse.code_Bg2 = theme.Rows[0]["BgUri2"].ToString();
                                    tokenResponse.code_Bg3 = theme.Rows[0]["BgUri3"].ToString();
                                    tokenResponse.code_Bg4 = theme.Rows[0]["BgUri4"].ToString();
                                    tokenResponse.code_Bg5 = theme.Rows[0]["BgUri5"].ToString();
                                    tokenResponse.code_Navbar2 = theme.Rows[0]["NavbarColor2"].ToString();
                                    tokenResponse.PicGroupCompany = theme.Rows[0]["CompanyGroupLogo"].ToString();
                                    tokenResponse.contractnumber = dt.Rows[0]["contractnumber"].ToString();
                                    tokenResponse.returncode = "1000";

                                    var contract = dll.GetContract(dt.Rows[0]["contractnumber"].ToString());
                                    if (contract.Rows.Count != 0)
                                    {
                                        co_name = contract.Rows[0]["CompanyNameTH"].ToString();
                                        tokenResponse.Company = contract.Rows[0]["CompanyNameTH"].ToString();
                                        tokenResponse.Customername = contract.Rows[0]["BusinessPartnerName"].ToString();
                                        tokenResponse.Shopname = contract.Rows[0]["ShopName"].ToString();
                                        tokenResponse.Roomnumber = contract.Rows[0]["smart_room_no"].ToString();
                                    }

                                }
                                else
                                {
                                    tokenResponse.code_theme = "#CCA9DA";
                                    tokenResponse.code_Navbar = "#fff";
                                    tokenResponse.code_Bg = "#2d2339";
                                    tokenResponse.code_Bg2 = "#453658";
                                    tokenResponse.code_Bg3 = "#5f4a79";
                                    tokenResponse.code_Bg4 = "#7b5f9c";
                                    tokenResponse.code_Bg5 = "#9775c0";
                                    tokenResponse.code_Navbar2 = "#fff";
                                    tokenResponse.PicGroupCompany = "img/siampi.png";
                                }
                            }

                            var conDetail = dll.GetContractDetail(te.Rows[0]["contractnumber"].ToString());
                            if (conDetail.Rows.Count != 0)
                            {
                                shop_name = conDetail.Rows[0]["ShopName"].ToString();
                                room_number = conDetail.Rows[0]["smart_room_no"].ToString();//
                                business_name = conDetail.Rows[0]["BUsinessPartnerName"].ToString();//
                                floor = conDetail.Rows[0]["smart_floor"].ToString();//

                                var u_ = dll.GetUserByUserid(conDetail.Rows[0]["smart_icon_staff_id"].ToString());
                                if (u_.Rows.Count != 0)
                                {
                                    email_ar = u_.Rows[0]["email"].ToString();
                                    ar_name = u_.Rows[0]["Fname"].ToString() + " " + u_.Rows[0]["Lname"].ToString();
                                }
                            }

                            mail.CallMail_forget_password(ar_name, co_name, shop_name, business_name, floor, room_number,
                                DateTime.Now.ToString("dd/MM/yyyy", EngCI), email_ar, conDetail.Rows[0]["CompanyCode"].ToString());


                        }
                        else
                        {
                            tokenResponse.returncode = "3800";

                        }
                    }
                    catch (Exception ex)
                    {
                        tokenResponse.Company = ex.ToString();
                    }
                }
            }


            return Ok(tokenResponse);
        }


        public String ConvertImageURLToBase64(String ID)
        {

            string url = "http://10.10.6.20:8162" + ID.Replace("~", "");
            //create an object of StringBuilder type.
            StringBuilder _sb = new StringBuilder();
            //create a byte array that will hold the return value of the getImg method
            Byte[] _byte = this.GetImg(url);
            //appends the argument to the stringbulilder object (_sb)
            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));
            //return the complete and final url in a base64 format.
            return string.Format(@"data:image/png;base64,{0}", _sb.ToString());

        }

        private byte[] GetImg(string url)
        {
            //create a stream object and initialize it to null
            Stream stream = null;
            //create a byte[] object. It serves as a buffer.
            byte[] buf;
            try
            {
                //Create a new WebProxy object.
                WebProxy myProxy = new WebProxy();
                //create a HttpWebRequest object and initialize it by passing the colleague api url to a create method.
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                //Create a HttpWebResponse object and initilize it
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                //get the response stream
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    //get the content length in integer
                    int len = (int)(response.ContentLength);
                    //Read bytes
                    buf = br.ReadBytes(len);
                    //close the binary reader
                    br.Close();
                }
                //close the stream object
                stream.Close();
                //close the response object 
                response.Close();
            }
            catch (Exception exp)
            {
                //set the buffer to null
                buf = null;
            }
            //return the buffer
            return (buf);
        }

        [HttpPost]
        [Route("api/getlogo")]
        public ret_logo getlogo()
        {

            ret_logo ret = new ret_logo();
            loginDLL dll = new loginDLL();
            DataTable dt = new DataTable();

            try
            {
                dt = dll.Get_Pic_Logo_login();

                if (dt.Rows.Count != 0)
                {
                    if (dt.Rows.Count == 3)
                    {
                        //ret.image1 = ConBase64(dt.Rows[0]["CompanyGroupLogo"].ToString());
                        ret.image1 = ConvertImageURLToBase64(dt.Rows[0]["CompanyGroupLogo"].ToString());
                        ret.image2 = ConvertImageURLToBase64(dt.Rows[1]["CompanyGroupLogo"].ToString());
                        ret.image3 = ConvertImageURLToBase64(dt.Rows[2]["CompanyGroupLogo"].ToString());
                        ret.desc = "success v.20200318.1";
                        ret.returncode = "1000";
                    }
                    else if (dt.Rows.Count == 2)
                    {
                        ret.image1 = ConvertImageURLToBase64(dt.Rows[0]["CompanyGroupLogo"].ToString());
                        ret.image2 = ConvertImageURLToBase64(dt.Rows[1]["CompanyGroupLogo"].ToString());
                        ret.image3 = "";
                        ret.desc = "success v.20200318.1";
                        ret.returncode = "1000";
                    }
                    else if (dt.Rows.Count == 1)
                    {
                        ret.image1 = ConvertImageURLToBase64(dt.Rows[0]["CompanyGroupLogo"].ToString());
                        ret.image2 = "";
                        ret.image3 = "";
                        ret.desc = "success v.20200318.1";
                        ret.returncode = "1000";
                    }

                }
                else
                {
                    ret.image1 = "";
                    ret.image2 = "";
                    ret.image3 = "";
                    ret.returncode = "3800";
                    ret.desc = "Logo Not Found";
                }
            }
            catch (Exception ex)
            {
                ret.image1 = "";
                ret.image2 = "";
                ret.image3 = "";
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }


        public class change_password_req
        {
            public string username { get; set; }
            public string password { get; set; }
            public string oldpassword { get; set; }
            public string contractnumber { get; set; }
            public string userid { get; set; }

        }

        public class ret_change_password
        {
            public string desc { get; set; }
            public string returncode { get; set; }
        }


        [Authorize]
        [HttpPost]
        [Route("api/change_password")]
        public ret_change_password change_password(change_password_req param)
        {

            ret_change_password ret = new ret_change_password();
            loginDLL dll = new loginDLL();

            try
            {
                var u = dll.getUser_tenant(param.username, param.oldpassword);
                if (u.Rows.Count != 0)
                {
                    dll.Update_Password(param.username, param.contractnumber, param.password, param.userid);

                    ret.desc = "success";
                    ret.returncode = "1000";
                }
                else
                {
                    ret.desc = "Password เก่าไม่ถูกต้อง";
                    ret.returncode = "3800";
                }




            }
            catch (Exception ex)
            {
                ret.desc = ex.ToString();
                ret.returncode = "3800";
            }

            return ret;
        }




    }
}
