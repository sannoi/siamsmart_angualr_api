﻿using siamsmartcollection_tenant.App_Code;
using siamsmartcollection_tenant.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace siamsmartcollection_tenant.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class transactionController : ApiController
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        public class req_transaction
        {
            public string ContractNumber { get; set; }
            public string Status { get; set; }
            public string From_date { get; set; }
            public string End_date { get; set; }
        }

        public class ret_transaction
        {
            public string samrt_chkflag_confirm { get; set; }
            public string id { get; set; }
            public string record_date { get; set; }
            public string ContractNumber { get; set; }
            public string product_amount { get; set; }
            public string service_serCharge_amount { get; set; }
            public string status { get; set; }
            public string flag_confirm { get; set; }
            public string returncode { get; set; }
            public string desc { get; set; }
            public Boolean chk { get; set; }

        }

        public string con_date(string x)
        {
            string result = "";

            result = x.Substring(6, 4) + "-" + x.Substring(3, 2) + "-" + x.Substring(0, 2);

            return result;
        }

        [Authorize]
        [HttpPost]
        [Route("api/getTransaction")]
        public List<ret_transaction> getTransaction(req_transaction param)
        {
            List<ret_transaction> ret = new List<ret_transaction>();
            transactionDLL dll = new transactionDLL();
            DataTable dt = new DataTable();

            try
            {
                string samrt_chkflag_confirm = "";


                var c = dll.Get_Confirm_new(param.ContractNumber.Substring(0, 3));
                var c2 = dll.Get_Confirm_new2(param.ContractNumber.Substring(0, 3));

                if (c.Rows.Count != 0)
                {
                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (c2.Rows.Count != 0)
                    {
                        if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else
                        {
                            samrt_chkflag_confirm = "n";
                        }
                    }
                    else
                    {
                        samrt_chkflag_confirm = "n";
                    }

                }
                else
                {
                    samrt_chkflag_confirm = "n";
                }


                string contractNUmber = "";

                var conOld = dll.getOldContract(param.ContractNumber);
                if (conOld.Rows.Count != 0)
                {
                    contractNUmber += "" + conOld.Rows[0]["Contract_old_number"].ToString() + ",";

                    var c_ = dll.getOldContract(conOld.Rows[0]["Contract_old_number"].ToString());
                    if (c_.Rows.Count != 0)
                    {
                        contractNUmber += "" + c_.Rows[0]["Contract_old_number"].ToString() + ",";
                    }
                }

                contractNUmber += "" + param.ContractNumber + "";

                var t = dll.GetTransaction(contractNUmber, con_date(param.From_date), con_date(param.End_date), param.Status);
                if (t.Rows.Count != 0)
                {
                    for (int i = 0; i < t.Rows.Count; i++)
                    {
                        ret_transaction x = new ret_transaction();

                        x.ContractNumber = t.Rows[i]["ContractNumber"].ToString();
                        x.id = t.Rows[i]["id"].ToString();
                        x.flag_confirm = t.Rows[i]["flag_confirm"].ToString();
                        x.product_amount = t.Rows[i]["product_amount"].ToString();
                        x.record_date = t.Rows[i]["record_date"].ToString();
                        x.samrt_chkflag_confirm = samrt_chkflag_confirm;
                        x.service_serCharge_amount = t.Rows[i]["service_serCharge_amount"].ToString();
                        x.status = t.Rows[i]["status"].ToString();
                        x.returncode = "1000";
                        x.desc = "success";
                        x.chk = false;

                        ret.Add(x);
                    }
                }
                else
                {
                    ret_transaction x = new ret_transaction();

                    x.ContractNumber = "";
                    x.id = "";
                    x.flag_confirm = "";
                    x.product_amount = "";
                    x.record_date = "";
                    x.samrt_chkflag_confirm = samrt_chkflag_confirm;
                    x.service_serCharge_amount = "";
                    x.status = "";
                    x.returncode = "1000";
                    x.desc = "success";

                    ret.Add(x);
                }

            }
            catch (Exception ex)
            {
                ret_transaction x = new ret_transaction();

                x.ContractNumber = "";
                x.id = "";
                x.flag_confirm = "";
                x.product_amount = "";
                x.record_date = "";
                x.samrt_chkflag_confirm = "";
                x.service_serCharge_amount = "";
                x.status = "";
                x.returncode = "3800";
                x.desc = ex.ToString();

                ret.Add(x);
            }

            return ret;
        }

        [Authorize]
        [HttpPost]
        [Route("api/getTransaction_all")]
        public List<ret_transaction> getTransaction_all(req_transaction param)
        {
            List<ret_transaction> ret = new List<ret_transaction>();
            transactionDLL dll = new transactionDLL();
            DataTable dt = new DataTable();

            try
            {
                string samrt_chkflag_confirm = "";


                var c = dll.Get_Confirm_new(param.ContractNumber.Substring(0, 3));
                var c2 = dll.Get_Confirm_new2(param.ContractNumber.Substring(0, 3));

                if (c.Rows.Count != 0)
                {
                    if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                    {
                        samrt_chkflag_confirm = "y";
                    }
                    else if (c2.Rows.Count != 0)
                    {
                        if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(1).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(2).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(3).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(4).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(5).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else if (DateTime.Now.ToString("yyyy-MM-dd", EngCI) == Convert.ToDateTime(c2.Rows[0]["jobnext_datetime"]).AddMonths(-1).AddDays(6).ToString("yyyy-MM-dd", EngCI))
                        {
                            samrt_chkflag_confirm = "y";
                        }
                        else
                        {
                            samrt_chkflag_confirm = "n";
                        }
                    }
                    else
                    {
                        samrt_chkflag_confirm = "n";
                    }

                }
                else
                {
                    samrt_chkflag_confirm = "n";
                }


                string contractNUmber = "";

                var conOld = dll.getOldContract(param.ContractNumber);
                if (conOld.Rows.Count != 0)
                {
                    contractNUmber += "'" + conOld.Rows[0]["Contract_old_number"].ToString() + "',";

                    var c_ = dll.getOldContract(conOld.Rows[0]["Contract_old_number"].ToString());
                    if (c_.Rows.Count != 0)
                    {
                        contractNUmber += "'" + c_.Rows[0]["Contract_old_number"].ToString() + "',";
                    }
                }

                contractNUmber += "" + param.ContractNumber + "";

                var t = dll.GetTransaction(contractNUmber, con_date(param.From_date), con_date(param.End_date), param.Status);
                if (t.Rows.Count != 0)
                {
                    for (int i = 0; i < t.Rows.Count; i++)
                    {
                        ret_transaction x = new ret_transaction();

                        x.ContractNumber = t.Rows[i]["ContractNumber"].ToString();
                        x.id = t.Rows[i]["id"].ToString();
                        x.flag_confirm = t.Rows[i]["flag_confirm"].ToString();
                        x.product_amount = t.Rows[i]["product_amount"].ToString();
                        x.record_date = t.Rows[i]["record_date"].ToString();
                        x.samrt_chkflag_confirm = samrt_chkflag_confirm;
                        x.service_serCharge_amount = t.Rows[i]["service_serCharge_amount"].ToString();
                        x.status = t.Rows[i]["status"].ToString();
                        x.returncode = "1000";
                        x.desc = "success";
                        x.chk = true;

                        ret.Add(x);
                    }
                }
                else
                {
                    ret_transaction x = new ret_transaction();

                    x.ContractNumber = "";
                    x.id = "";
                    x.flag_confirm = "";
                    x.product_amount = "";
                    x.record_date = "";
                    x.samrt_chkflag_confirm = samrt_chkflag_confirm;
                    x.service_serCharge_amount = "";
                    x.status = "";
                    x.returncode = "1000";
                    x.desc = "success";

                    ret.Add(x);
                }

            }
            catch (Exception ex)
            {
                ret_transaction x = new ret_transaction();

                x.ContractNumber = "";
                x.id = "";
                x.flag_confirm = "";
                x.product_amount = "";
                x.record_date = "";
                x.samrt_chkflag_confirm = "";
                x.service_serCharge_amount = "";
                x.status = "";
                x.returncode = "3800";
                x.desc = ex.ToString();

                ret.Add(x);
            }

            return ret;
        }


        public class req_confirmTransaction
        {
            public string ContractNumber { get; set; }
            public string[] id { get; set; }
        }

        public class ret_confirmTransaction
        {
            public string returncode { get; set; }
            public string desc { get; set; }
        }


        [Authorize]
        [HttpPost]
        [Route("api/confirmTransaction")]
        public ret_confirmTransaction confirmTransaction(req_confirmTransaction param)
        {
            ret_confirmTransaction ret = new ret_confirmTransaction();
            transactionDLL dll = new transactionDLL();

            try
            {
                if (param.id.Length != 0)
                {
                    for (int i = 0; i < param.id.Length; i++)
                    {
                        dll.Update_flagConfirm(param.ContractNumber, param.id[i]);
                    }
                }


                ret.returncode = "1000";
                ret.desc = "success";

            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }

        public class req_recordTransaction_contract
        {
            public string ContractNumber { get; set; }
            public string id { get; set; }
        }

        public class ret_recordTransaction_contract
        {
            public string smart_room_no { get; set; }
            public string smart_shop_open { get; set; }
            public string smart_shop_close { get; set; }
            public string ShopName { get; set; }
            public string smart_record_keyin_type { get; set; }
            public string record_date { get; set; }
            public string product_amount { get; set; }
            public string tran_status { get; set; }
            public string no_type { get; set; }
            public string remark { get; set; }
            public string flag_confirm { get; set; }
            public string service_serCharge_amount { get; set; }
            public string returncode { get; set; }
            public string desc { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Transaction_contract_detail")]
        public ret_recordTransaction_contract Transaction_contract_detail(req_recordTransaction_contract param)
        {
            ret_recordTransaction_contract ret = new ret_recordTransaction_contract();
            transactionDLL dll = new transactionDLL();
            DataTable dt = new DataTable();

            try
            {
                var contract = dll.GetContract(param.ContractNumber);
                if (contract.Rows.Count != 0)
                {
                    ret.smart_room_no = contract.Rows[0]["smart_room_no"].ToString();
                    ret.ShopName = contract.Rows[0]["ShopName"].ToString();
                    ret.smart_record_keyin_type = contract.Rows[0]["smart_record_keyin_type"].ToString();
                    ret.smart_shop_open = contract.Rows[0]["smart_shop_open"].ToString();
                    ret.smart_shop_close = contract.Rows[0]["smart_shop_close"].ToString();

                    ret.returncode = "1000";
                    ret.desc = "success";

                    if (param.id != "")
                    {
                        var trans = dll.GetTransaction_detail(param.id, param.ContractNumber);
                        if (trans.Rows.Count != 0)
                        {
                            ret.record_date = trans.Rows[0]["record_date"].ToString();
                            ret.product_amount = trans.Rows[0]["product_amount"].ToString();
                            ret.tran_status = trans.Rows[0]["status"].ToString();
                            ret.no_type = trans.Rows[0]["no_type"].ToString();

                            if (trans.Rows[0]["flag_adjust"].ToString() == "y")
                            {
                                ret.remark = trans.Rows[0]["adjust_remark"].ToString();
                            }
                            else
                            {
                                ret.remark = trans.Rows[0]["remark"].ToString();
                            }

                            ret.flag_confirm = trans.Rows[0]["flag_confirm"].ToString();
                            ret.service_serCharge_amount = trans.Rows[0]["service_serCharge_amount"].ToString();
                        }
                    }
                    else
                    {
                        ret.record_date = "";
                        ret.product_amount = "";
                        ret.tran_status = "";
                        ret.no_type = "";
                        ret.remark = "";
                        ret.flag_confirm = "";
                        ret.service_serCharge_amount = "";
                    }

                }
            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }


        public class req_recordTransaction
        {
            public string idTransaction { get; set; }
            public string date { get; set; }
            public string ContractNumber { get; set; }
            public string reason { get; set; }
            public string tran_status { get; set; }
            public string remark { get; set; }
            public string userid { get; set; }
        }


        public class ret_recordTransaction
        {
            public string desc { get; set; }
            public string returncode { get; set; }
            public string lastID { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Update_recordTransaction_0")]
        public ret_recordTransaction recordTransaction(req_recordTransaction param)
        {
            ret_recordTransaction ret = new ret_recordTransaction();
            transactionDLL dll = new transactionDLL();

            try
            {
                if (param.reason != "")
                {
                    if (param.tran_status == "y")
                    {

                        dll.Update_transaction_recor_1(param.ContractNumber, "0", "0", "y", con_date(param.date), param.reason
                                    , param.remark, param.idTransaction, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                                    param.userid, 0, "te", "web");

                    }
                    else
                    {

                        dll.Update_transaction_recor_createDate_Stamp(param.ContractNumber, "0", "0", "y", con_date(param.date), param.reason
                              , param.remark, param.idTransaction, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI),
                              param.userid, 0, "te", "web");
                    }

                    ret.returncode = "1000";
                    ret.desc = "success";
                }

            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }

        public class req_insert_recordTransaction_0
        {
            public string date { get; set; }
            public string ContractNumber { get; set; }
            public string reason { get; set; }
            public string remark { get; set; }
            public string userid { get; set; }
            public string smart_record_keyin_type { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("api/insert_recordTransaction_0")]
        public ret_recordTransaction insert_recordTransaction_0(req_insert_recordTransaction_0 param)
        {
            ret_recordTransaction ret = new ret_recordTransaction();
            transactionDLL dll = new transactionDLL();

            try
            {
                var indent = dll.Insert_transaction_record(param.ContractNumber, "0", "0", "y", con_date(param.date), param.reason
                        , param.remark, 0, param.smart_record_keyin_type, param.userid, "te", "web");

                ret.returncode = "1000";
                ret.desc = "success";

            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }

        public class req_chkDupTransaction
        {
            public string date { get; set; }
            public string ContractNumber { get; set; }
        }

        public class ret_chkDupTransaction
        {
            public string desc { get; set; }
            public string message { get; set; }
            public string returncode { get; set; }
            public string transactionid { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("api/chkDupTransaction")]
        public ret_chkDupTransaction chkDupTransaction(req_chkDupTransaction param)
        {
            ret_chkDupTransaction ret = new ret_chkDupTransaction();
            transactionDLL dll = new transactionDLL();

            try
            {
                var exist = dll.GetTransactionByDate(con_date(param.date), param.ContractNumber);
                if (exist.Rows.Count != 0)
                {
                    if (exist.Rows[0]["status"].ToString() == "y")
                    {
                        ret.desc = "dup";
                        ret.returncode = "1000";
                        ret.message = "วันที่ที่เลือกมีการคีย์ยอดแล้ว";
                    }
                    else if (exist.Rows[0]["status"].ToString() == "n")
                    {
                        ret.desc = "ok-update";
                        ret.returncode = "1000";
                        ret.message = "";
                        ret.transactionid = exist.Rows[0]["id"].ToString();
                    }
                }
                else
                {
                    ret.desc = "ok-insert";
                    ret.returncode = "1000";
                    ret.message = "";
                }

            }
            catch (Exception ex)
            {
                ret.desc = ex.ToString();
                ret.returncode = "3800";
                ret.message = "";
            }

            return ret;
        }

        public class img_result
        {
            public string filename { get; set; }
            public string code { get; set; }

        }

        public class Uploadimg_response
        {
            public string img_icon { get; set; }
            public string img_name { get; set; }
            public string filename { get; set; }

        }


        [Route("api/Files/Uploadimg")]
        [AllowAnonymous]
        public Uploadimg_response PostUserImage()
        {
            Uploadimg_response a = new Uploadimg_response();


            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {

                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];

                    Random _r = new Random();
                    //int n = _r.Next();
                    String n = _r.Next(0, 999999).ToString("D6");
                    string tmp_ = "m_" + n + "_" + DateTime.Now.ToString("yyyyMMddssFFF") + postedFile.FileName;
                    img_result b = new img_result();


                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 5; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 5 mb.");
                        }
                        else
                        {

                            if (Directory.Exists(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd"))) == false)
                            {
                                System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd")));
                            }


                            var filePath = HttpContext.Current.Server.MapPath("~/img/slip/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_);
                            postedFile.SaveAs(filePath);
                            var message1 = string.Format(DateTime.Now.ToString("yyyy-MM-dd") + "/" + tmp_);

                            a.img_name = message1;
                            a.img_icon = "/img/img.png";
                            a.filename = postedFile.FileName;
                        }
                    }


                }

            }
            catch (Exception ex)
            {
                a.img_name = ex.ToString();
            }

            return a;
        }

        public class req_Update_recordTransaction
        {
            public string idTransaction { get; set; }
            public string date { get; set; }
            public string ContractNumber { get; set; }
            public string tran_status { get; set; }
            public string userid { get; set; }
            public string product_amount { get; set; }
            public string service_serCharge_amount { get; set; }
            //public string img_icon { get; set; }
            //public string img_names { get; set; }
            //public string filename { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("api/Update_recordTransaction")]
        public ret_recordTransaction Update_recordTransaction(req_Update_recordTransaction param)
        {
            ret_recordTransaction ret = new ret_recordTransaction();
            transactionDLL dll = new transactionDLL();

            try
            {
                int vat = 0;
                var v = dll.GetVAT(param.ContractNumber.Substring(0, 3));
                if (v.Rows.Count != 0)
                {
                    vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
                }
                else
                {
                    vat = 7;
                }

                if (param.tran_status == "y")
                {

                    dll.Update_transaction_recor_1(param.ContractNumber, param.product_amount, param.service_serCharge_amount, "y", con_date(param.date), ""
                                       , "", param.idTransaction, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), param.userid, vat, "te", "web");

                }
                else
                {

                    dll.Update_transaction_recor_createDate_Stamp(param.ContractNumber, param.product_amount, param.service_serCharge_amount, "y", con_date(param.date),
                                   "", "", param.idTransaction, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", EngCI), param.userid, vat, "te", "web");
                }

                //dll.tbltransaction_record_img(param.idTransaction, param.img_icon, param.img_names, param.filename);

                ret.returncode = "1000";
                ret.desc = "success";
                ret.lastID = param.idTransaction;
            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }

        public class req_insert_recordTransaction
        {
            public string date { get; set; }
            public string ContractNumber { get; set; }
            public string userid { get; set; }
            public string smart_record_keyin_type { get; set; }
            public string product_amount { get; set; }
            public string service_serCharge_amount { get; set; }
            //public string img_icon { get; set; }
            //public string img_names { get; set; }
            //public string filename { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("api/insert_recordTransaction")]
        public ret_recordTransaction insert_recordTransaction(req_insert_recordTransaction param)
        {
            ret_recordTransaction ret = new ret_recordTransaction();
            transactionDLL dll = new transactionDLL();

            try
            {
                int vat = 0;
                var v = dll.GetVAT(param.ContractNumber.Substring(0, 3));
                if (v.Rows.Count != 0)
                {
                    vat = Convert.ToInt32(v.Rows[0]["vat"].ToString());
                }
                else
                {
                    vat = 7;
                }

                var indent = dll.Insert_transaction_record(param.ContractNumber, param.product_amount, param.service_serCharge_amount, "y", con_date(param.date),
                              "", "", vat, param.smart_record_keyin_type, param.userid, "te", "web");

                //dll.tbltransaction_record_img(indent.Rows[0]["last_id"].ToString(), param.img_icon, param.img_names, param.filename);

                ret.returncode = "1000";
                ret.desc = "success";
                ret.lastID = indent.Rows[0]["last_id"].ToString();

            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }

        public class req_insert_imgTransaction
        {
            public string idTransaction { get; set; }
            public string img_icon { get; set; }
            public string img_names { get; set; }
            public string filename { get; set; }
            public string userid { get; set; }
            public string file_type { get; set; }
            public string Contract_number { get; set; }
        }


        [Authorize]
        [HttpPost]
        [Route("api/insert_imgTransaction")]
        public ret_recordTransaction insert_imgTransaction(req_insert_imgTransaction param)
        {
            ret_recordTransaction ret = new ret_recordTransaction();
            transactionDLL dll = new transactionDLL();

            try
            {

                string icon = "";
                if (param.file_type.ToLower().Replace(".", "") == "jpg" || param.file_type.ToLower().Replace(".", "") == "gif" || param.file_type.ToLower().Replace(".", "") == "png")
                {
                    icon = "/img/img.png";
                }
                else if (param.file_type.ToLower().Replace(".", "") == "doc" || param.file_type.ToLower().Replace(".", "") == "docx")
                {
                    icon = "/img/doc.png";

                }
                else if (param.file_type.ToLower().Replace(".", "") == "xls" || param.file_type.ToLower().Replace(".", "") == "xlsx")
                {
                    icon = "/img/xls.png";

                }
                else if (param.file_type.ToLower().Replace(".", "") == "txt")
                {
                    icon = "/img/txt.png";

                }
                else if (param.file_type.ToLower().Replace(".", "") == "pdf")
                {
                    icon = "/img/pdf.png";
                }

                dll.tbltransaction_record_img(param.idTransaction, icon, param.img_names,
                    param.Contract_number + "w_" + "_" + DateTime.Now.ToString("yyyyMMddssFFF"));

                ret.returncode = "1000";
                ret.desc = "success";
            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }

        public class req_getImage_transaction
        {
            public string idTransaction { get; set; }
        }

        public class req_getImage_transactionByid
        {
            public string id_img { get; set; }
        }

        public class ret_getImage_transaction
        {
            public string img { get; set; }
            public string filename { get; set; }
            public string id { get; set; }
            public string filename_display { get; set; }
            public string file_type { get; set; }
        }



        public String ConvertImageURLToBase64(String ID)
        {

            string url = "http://10.10.6.20:8162/img/slip/" + ID.Replace("~", "");
            //create an object of StringBuilder type.
            StringBuilder _sb = new StringBuilder();
            //create a byte array that will hold the return value of the getImg method
            Byte[] _byte = this.GetImg(url);
            //appends the argument to the stringbulilder object (_sb)
            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));
            //return the complete and final url in a base64 format.
            return string.Format(@"data:image/png;base64,{0}", _sb.ToString());

        }

        private byte[] GetImg(string url)
        {
            //create a stream object and initialize it to null
            Stream stream = null;
            //create a byte[] object. It serves as a buffer.
            byte[] buf;
            try
            {
                //Create a new WebProxy object.
                WebProxy myProxy = new WebProxy();
                //create a HttpWebRequest object and initialize it by passing the colleague api url to a create method.
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                //Create a HttpWebResponse object and initilize it
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                //get the response stream
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    //get the content length in integer
                    int len = (int)(response.ContentLength);
                    //Read bytes
                    buf = br.ReadBytes(len);
                    //close the binary reader
                    br.Close();
                }
                //close the stream object
                stream.Close();
                //close the response object 
                response.Close();
            }
            catch (Exception exp)
            {
                //set the buffer to null
                buf = null;
            }
            //return the buffer
            return (buf);
        }


        [Authorize]
        [HttpPost]
        [Route("api/getImage_transaction")]
        public List<ret_getImage_transaction> getImage_transaction(req_getImage_transaction param)
        {
            List<ret_getImage_transaction> ret = new List<ret_getImage_transaction>();
            transactionDLL dll = new transactionDLL();

            try
            {
                var img = dll.GetTransaction_detail_img(param.idTransaction);
                if (img.Rows.Count != 0)
                {
                    for (int i = 0; i < img.Rows.Count; i++)
                    {
                        ret_getImage_transaction x = new ret_getImage_transaction();
                        x.img = img.Rows[i]["img"].ToString();
                        x.filename = "";// ConvertImageURLToBase64(img.Rows[i]["filename"].ToString());
                        x.id = img.Rows[i]["id"].ToString();
                        x.filename_display = img.Rows[i]["filename_display"].ToString();
                        x.file_type = img.Rows[i]["img"].ToString().Replace("/img/", "").Replace(".png", "");

                        ret.Add(x);

                    }
                }
            }
            catch (Exception ex)
            {
                ret_getImage_transaction x = new ret_getImage_transaction();
                x.img = "";
                x.filename = "";
                x.id = "";
                x.filename_display = "";

                ret.Add(x);
            }

            return ret;
        }


        [Authorize]
        [HttpPost]
        [Route("api/getImage_transaction_adjust")]
        public List<ret_getImage_transaction> getImage_transaction_adjust(req_getImage_transaction param)
        {
            List<ret_getImage_transaction> ret = new List<ret_getImage_transaction>();
            transactionDLL dll = new transactionDLL();

            try
            {
                var img = dll.GetTransaction_detail_edit_img(param.idTransaction);
                if (img.Rows.Count != 0)
                {
                    for (int i = 0; i < img.Rows.Count; i++)
                    {
                        ret_getImage_transaction x = new ret_getImage_transaction();
                        x.img = img.Rows[i]["img"].ToString();
                        x.filename = ""; // ConvertImageURLToBase64(img.Rows[i]["filename"].ToString());
                        x.id = img.Rows[i]["id"].ToString();
                        x.filename_display = img.Rows[i]["filename_display"].ToString();
                        x.file_type = img.Rows[i]["img"].ToString().Replace("/img/", "").Replace(".png", "");

                        ret.Add(x);

                    }
                }
            }
            catch (Exception ex)
            {
                ret_getImage_transaction x = new ret_getImage_transaction();
                x.img = "";
                x.filename = "";
                x.id = "";
                x.filename_display = "";

                ret.Add(x);
            }

            return ret;
        }


        [Authorize]
        [HttpPost]
        [Route("api/getImage_transactionByID")]
        public List<ret_getImage_transaction> getImage_transactionByID(req_getImage_transactionByid param)
        {
            List<ret_getImage_transaction> ret = new List<ret_getImage_transaction>();
            transactionDLL dll = new transactionDLL();

            try
            {
                var img = dll.GetTransaction_detail_imgByID(param.id_img);
                if (img.Rows.Count != 0)
                {
                    for (int i = 0; i < img.Rows.Count; i++)
                    {
                        ret_getImage_transaction x = new ret_getImage_transaction();
                        x.img = img.Rows[i]["img"].ToString();
                        x.filename = ConvertImageURLToBase64(img.Rows[i]["filename"].ToString());
                        x.id = img.Rows[i]["id"].ToString();
                        x.filename_display = img.Rows[i]["filename_display"].ToString();
                        x.file_type = img.Rows[i]["img"].ToString().Replace("/img/", "").Replace(".png", "");

                        ret.Add(x);

                    }
                }
            }
            catch (Exception ex)
            {
                ret_getImage_transaction x = new ret_getImage_transaction();
                x.img = "";
                x.filename = "";
                x.id = "";
                x.filename_display = "";

                ret.Add(x);
            }

            return ret;
        }


        [Authorize]
        [HttpPost]
        [Route("api/getImage_transaction_adjustByID")]
        public List<ret_getImage_transaction> getImage_transaction_adjustByID(req_getImage_transactionByid param)
        {
            List<ret_getImage_transaction> ret = new List<ret_getImage_transaction>();
            transactionDLL dll = new transactionDLL();

            try
            {
                var img = dll.GetTransaction_detail_edit_imgByID(param.id_img);
                if (img.Rows.Count != 0)
                {
                    for (int i = 0; i < img.Rows.Count; i++)
                    {
                        ret_getImage_transaction x = new ret_getImage_transaction();
                        x.img = img.Rows[i]["img"].ToString();
                        x.filename = ConvertImageURLToBase64(img.Rows[i]["filename"].ToString());
                        x.id = img.Rows[i]["id"].ToString();
                        x.filename_display = img.Rows[i]["filename_display"].ToString();
                        x.file_type = img.Rows[i]["img"].ToString().Replace("/img/", "").Replace(".png", "");

                        ret.Add(x);

                    }
                }
            }
            catch (Exception ex)
            {
                ret_getImage_transaction x = new ret_getImage_transaction();
                x.img = "";
                x.filename = "";
                x.id = "";
                x.filename_display = "";

                ret.Add(x);
            }

            return ret;
        }



        public class req_adjust_transaction
        {
            public string idTransaction { get; set; }
            public string ContractNumber { get; set; }
            public string remark { get; set; }
            public string userid { get; set; }
            public string date { get; set; }
            public string shopname { get; set; }
            public string roomno { get; set; }
        }

        [Authorize]
        [HttpPost]
        [Route("api/adjust_transaction")]
        public ret_recordTransaction adjust_transaction(req_adjust_transaction param)
        {
            ret_recordTransaction ret = new ret_recordTransaction();
            transactionDLL dll = new transactionDLL();
            sentmail m = new sentmail();
            try
            {

                dll.Update_Transaction(param.remark, param.idTransaction, param.ContractNumber, param.userid, "te");

                var cont = dll.GetContract(param.ContractNumber);
                if (cont.Rows.Count != 0)
                {
                    var u = dll.GetUser(cont.Rows[0]["smart_icon_staff_id"].ToString());
                    if (u.Rows.Count != 0)
                    {


                        string comname_ = "";
                        var comname = dll.GetCompanyCodeByCompanyCode(cont.Rows[0]["CompanyCode"].ToString());
                        if (comname.Rows.Count != 0)
                        {
                            comname_ = comname.Rows[0]["CompanyNameEN"].ToString();
                        }

                        m.CallMail_adjust(u.Rows[0]["email"].ToString(),
                             "ระบบ Tenant Sales Data Collection : แก้ไขยอดขายของวันที่ " + param.date + " ร้าน " + param.shopname + " ห้อง " + param.roomno,
                             "Tenant Sales Data Collection", u.Rows[0]["Fname"].ToString(),
                             "แก้ไขยอดขายของวันที่ " + param.date + " ร้าน " + param.shopname + " ห้อง " + param.roomno,

                             comname_, "ชื่อร้าน", "ชื่อบริษัท", "ชั้น", "ห้อง", "วันที่แก้ไขยอด", "Link",
                             "", cont.Rows[0]["ShopName"].ToString(), cont.Rows[0]["BusinessPartnerName"].ToString(),
                             cont.Rows[0]["smart_floor"].ToString(), cont.Rows[0]["smart_room_no"].ToString(),
                              param.date, "", param.remark, cont.Rows[0]["CompanyCode"].ToString());
                    }
                }

                ret.returncode = "1000";
                ret.desc = "success";
            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }

        [Authorize]
        [HttpPost]
        [Route("api/insert_imgTransaction_adjust")]
        public ret_recordTransaction insert_imgTransaction_adjust(req_insert_imgTransaction param)
        {
            ret_recordTransaction ret = new ret_recordTransaction();
            transactionDLL dll = new transactionDLL();

            try
            {
                string icon = "";
                if (param.file_type.ToLower().Replace(".", "") == "jpg" || param.file_type.ToLower().Replace(".", "") == "gif" || param.file_type.ToLower().Replace(".", "") == "png")
                {
                    icon = "/img/img.png";
                }
                else if (param.file_type.ToLower().Replace(".", "") == "doc" || param.file_type.ToLower().Replace(".", "") == "docx")
                {
                    icon = "/img/doc.png";

                }
                else if (param.file_type.ToLower().Replace(".", "") == "xls" || param.file_type.ToLower().Replace(".", "") == "xlsx")
                {
                    icon = "/img/xls.png";

                }
                else if (param.file_type.ToLower().Replace(".", "") == "txt")
                {
                    icon = "/img/txt.png";

                }
                else if (param.file_type.ToLower().Replace(".", "") == "pdf")
                {
                    icon = "/img/pdf.png";
                }


                dll.tbl_edit_transaction_record_img(param.idTransaction, icon, param.img_names,
                    param.Contract_number + "w_" + "_" + DateTime.Now.ToString("yyyyMMddssFFF"), param.userid);

                ret.returncode = "1000";
                ret.desc = "success";
            }
            catch (Exception ex)
            {
                ret.returncode = "3800";
                ret.desc = ex.ToString();
            }

            return ret;
        }

    }
}
